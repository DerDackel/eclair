package internal

import (
	"testing"
)

func TestDestructureImage(t *testing.T) {
	type args struct {
		image string
	}
	tests := []struct {
		name    string
		args    args
		want    *Image
		wantErr bool
	}{
		{
			name: "Happy Path",
			args: args{
				image: "http://localhost:5000/alpine:3.9",
			},
			want: &Image{
				Registry: NewRegistry("http://localhost:5000"),
				Repo:     "alpine",
				Tag:      "3.9",
			},
			wantErr: false,
		},
		{
			name: "No scheme",
			args: args{
				image: "localhost:5000/alpine:3.9",
			},
			want: &Image{
				Registry: NewRegistry("http://localhost:5000"),
				Repo:     "alpine",
				Tag:      "3.9",
			},
			wantErr: false,
		},
		{
			name: "No port",
			args: args{
				image: "registry.hub.docker.com/library/postgres:latest",
			},
			want: &Image{
				Registry: NewRegistry("http://registry.hub.docker.com:80"),
				Repo:     "library/postgres",
				Tag:      "latest",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DestructureImage(tt.args.image)
			if (err != nil) != tt.wantErr {
				t.Errorf("DestructureImage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got.Tag != tt.want.Tag {
				t.Errorf("DestructureImage().Tag = %v, want %v", got.Tag, tt.want.Tag)
			}
			if got.Repo != tt.want.Repo {
				t.Errorf("DestructureImage().Repo = %v, want %v", got.Repo, tt.want.Repo)
			}
			if got.Registry.Url != tt.want.Registry.Url {
				t.Errorf("DestructureImage().Registry.Url = %v, want %v", got.Registry.Url, tt.want.Registry.Url)
			}
		})
	}
}
