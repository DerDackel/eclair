package internal

import (
	"errors"
	v1 "github.com/coreos/clair/api/v1"
	"github.com/olekukonko/tablewriter"
	log "github.com/sirupsen/logrus"
	"regexp"
	"strings"
)

const imageRegex = `(?P<scheme>http[s]?)?(://)?(?P<registry>[.\w\d]+)(?P<port>:\d+)?(?P<repo>[/\w+]*):(?P<tag>.+)`

func init() {
	log.SetLevel(log.DebugLevel)
	log.SetFormatter(&log.JSONFormatter{})
}

func DestructureImage(image string) (*Image, error) {
	var result Image

	re := regexp.MustCompile(imageRegex)
	match := re.FindStringSubmatch(image)
	if match == nil {
		return nil, errors.New("unable to parse image name")
	}

	matchMap := make(map[string]string)
	for i, name := range re.SubexpNames() {
		if i != 0 && name != "" {
			matchMap[name] = match[i]
		}
	}

	scheme := "http"
	port := "80"
	if elem, ok := matchMap["scheme"]; ok && elem != "" {
		scheme = elem
		if elem == "https" {
			port = "443"
		}
	}
	if elem, ok := matchMap["port"]; ok && elem != "" {
		port = strings.TrimLeft(elem, ":")
	}
	registryPath := strings.Join([]string{scheme, "://", matchMap["registry"], ":", port}, "")
	result.Registry = NewRegistry(registryPath)
	result.Repo = strings.TrimLeft(matchMap["repo"], "/")
	result.Tag = matchMap["tag"]

	return &result, nil
}

func GenerateVulnerabilityTable(layers []*v1.Layer) string {
	tableString := &strings.Builder{}
	table := tablewriter.NewWriter(tableString)
	table.SetHeader([]string{"CVE Name", "CVE Severity", "Package Name", "Package Version", "CVE Description"})
	table.SetBorders(tablewriter.Border{Left: true, Top: false, Right: true, Bottom: false})
	table.SetCenterSeparator("|")

	var rows [][]string

	for _, layer := range layers {
		for _, feat := range layer.Features {
			for _, vuln := range feat.Vulnerabilities {
				row := []string{
					vuln.Name,
					vuln.Severity,
					feat.Name,
					feat.Version,
					vuln.Description,
				}
				rows = append(rows, row)
			}
		}
	}

	table.AppendBulk(rows)
	table.Render()

	return tableString.String()
}
