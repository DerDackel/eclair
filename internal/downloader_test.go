package internal

import (
	"crypto/sha256"
	"fmt"
	"net/http"
	"reflect"
	"testing"
)

const (
	clairUrl    = "http://localhost:6060"
	registryUrl = "http://10.39.73.99:5000"
)

func TestGetLayers(t *testing.T) {
	type args struct {
		image *Image
	}
	tests := []struct {
		name    string
		args    args
		want    []*LayerData
		wantErr bool
	}{
		{
			name: "Happy Path",
			args: args{
				image: &Image{
					Registry: NewRegistry(registryUrl),
					Repo:     "alpine",
					Tag:      "3.9",
				},
			},
			want: []*LayerData{{Sum: "sha256:a3ed95caeb02ffe68cdd9fd84406680ae93d633cb16422d00e8a7c22955b46d4", Parent: ""}, {Sum: "sha256:e7c96db7181be991f19a9fb6975cdbbd73c65f4a2681348e63a141a2192a5f10", Parent: "sha256:a3ed95caeb02ffe68cdd9fd84406680ae93d633cb16422d00e8a7c22955b46d4"}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			downloader := RegistryDownloader{
				client: http.DefaultClient,
			}
			got, err := downloader.GetLayerSums(tt.args.image)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetLayers() error = %v, error wanted: %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetLayers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRegistryDownloader_DownloadLayer(t *testing.T) {
	type args struct {
		image    *Image
		layerSum string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Happy Path",
			args: args{
				image: &Image{
					Registry: NewRegistry(registryUrl),
					Repo:     "alpine",
					Tag:      "3.9",
				},
				layerSum: "sha256:e7c96db7181be991f19a9fb6975cdbbd73c65f4a2681348e63a141a2192a5f10",
			},
			want:    "e7c96db7181be991f19a9fb6975cdbbd73c65f4a2681348e63a141a2192a5f10",
			wantErr: false,
		},
		{
			name: "404",
			args: args{
				image: &Image{
					Registry: NewRegistry(registryUrl),
					Repo:     "alpine",
					Tag:      "3.9",
				},
				layerSum: "fail",
			},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &RegistryDownloader{
				client: http.DefaultClient,
			}
			got, err := d.DownloadLayer(tt.args.image, tt.args.layerSum)
			if (err != nil) != tt.wantErr {
				t.Errorf("RegistryDownloader.DownloadLayer() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			var result string
			if got != nil {
				hasher := sha256.New()
				hasher.Write(got.Data)
				result = fmt.Sprintf("%x", hasher.Sum(nil))
			}
			if result != tt.want {
				t.Errorf("RegistryDownloader.DownloadLayer() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRegistryDownloader_SubmitLayerForCheck(t *testing.T) {

	type args struct {
		layerSum string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Happy Path",
			args: args{
				layerSum: "sha256:e7c96db7181be991f19a9fb6975cdbbd73c65f4a2681348e63a141a2192a5f10",
			},
			want:    "fail",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := RegistryDownloader{
				client: http.DefaultClient,
			}
			image := Image{
				Registry: &Registry{
					Url: registryUrl,
				},
				Repo: "alpine",
				Tag:  "3.9",
			}
			layer, err := d.DownloadLayer(&image, tt.args.layerSum)
			if err != nil {
				t.Errorf("Unable to obtain layer data, Reason: '%v'", err)
				return
			}
			err = d.SubmitLayerForCheck(&image, layer, clairUrl)
			if (err != nil) != tt.wantErr {
				t.Errorf("RegistryDownloader.SubmitLayerForCheck() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

		})
	}
}

func TestRegistryDownloader_GetVulnerabilities(t *testing.T) {
	type args struct {
		clairPath string
		layer     *LayerData
	}
	tests := []struct {
		name    string
		args    args
		want    []string
		wantErr bool
	}{
		{
			name: "Happy Path",
			args: args{
				clairPath: clairUrl,
				layer: &LayerData{
					Sum: "",
				},
			},
			want:    []string{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &RegistryDownloader{
				client: http.DefaultClient,
			}
			image := &Image{Registry: &Registry{Url: registryUrl}, Repo: "alpine", Tag: "3.9.3"}
			err := d.SubmitImage(image, tt.args.clairPath)
			if err != nil {
				t.Errorf("Unable to submit image for scanning: %v", err)
				return
			}
			got, err := d.GetLayerVulnerabilities(tt.args.clairPath, image.Layers[1])
			if (err != nil) != tt.wantErr {
				t.Errorf("RegistryDownloader.GetLayerVulnerabilities() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got == nil {
				t.Errorf("RegistryDownloader.GetLayerVulnerabilities() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRegistryDownloader_SubmitImage(t *testing.T) {
	type args struct {
		image     *Image
		clairPath string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Happy Path",
			args: args{
				image: &Image{
					Registry: &Registry{
						Url: registryUrl,
					},
					Repo: "alpine",
					Tag:  "3.9.3",
				},
				clairPath: clairUrl,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &RegistryDownloader{
				client: http.DefaultClient,
			}
			if err := d.SubmitImage(tt.args.image, tt.args.clairPath); (err != nil) != tt.wantErr {
				t.Errorf("RegistryDownloader.SubmitImage() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
