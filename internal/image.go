package internal

import "strings"

type Image struct {
	Registry *Registry
	Repo     string
	Tag      string
	Layers   []*LayerData
}

type Registry struct {
	Url string
}

type LayerData struct {
	Sum    string
	Parent string
	Data   []byte
}

func (i *Image) GetPath() string {
	return strings.Join([]string{i.Registry.Url, "v2", i.Repo}, "/")
}
