package internal

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	v1 "github.com/coreos/clair/api/v1"
	"io/ioutil"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"
)

type Downloader interface {
	GetLayerSums(image *Image) ([]*LayerData, error)
	DownloadLayer(image *Image, layerSum string) (*LayerData, error)
	SubmitLayerForCheck(image *Image, layer *LayerData, clairPath string) error
	SubmitImage(image *Image, clairPath string) error
	GetLayerVulnerabilities(clairPath string, layer *LayerData) (*v1.LayerEnvelope, error)
}

type RegistryDownloader struct {
	client *http.Client
}

func NewRegistryDownloader(client *http.Client) *RegistryDownloader {
	if client == nil {
		client = http.DefaultClient
	}
	return &RegistryDownloader{
		client: client,
	}
}

type manifestsResponse struct {
	FsLayers []struct {
		BlobSum string `json:"blobSum"`
	} `json:"fsLayers"`
}

func NewRegistry(url string) *Registry {
	return &Registry{
		Url: strings.TrimRight(strings.Replace(url, "/v2", "", 1), "/"),
	}
}

func (d *RegistryDownloader) GetLayerSums(image *Image) ([]*LayerData, error) {
	path := strings.Join([]string{image.GetPath(), "manifests", image.Tag}, "/")
	log.WithField("targetURL", path).Debug()
	req, err := http.NewRequest("GET", path, nil)
	if err != nil {
		return nil, err
	}
	resp, err := d.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)

	var manifests manifestsResponse
	err = json.Unmarshal(respBody, &manifests)
	if err != nil {
		return nil, err
	}
	var result []*LayerData
	for idx, layer := range manifests.FsLayers {
		if idx == 0 {
			result = append(result, &LayerData{Sum: layer.BlobSum, Parent: ""})
		} else {
			result = append(result, &LayerData{Sum: layer.BlobSum, Parent: manifests.FsLayers[idx-1].BlobSum})
		}

	}
	return result, nil
}

func (d *RegistryDownloader) DownloadLayer(image *Image, layerSum string) (*LayerData, error) {
	path := strings.Join([]string{image.GetPath(), "blobs", layerSum}, "/")
	log.WithField("targetURL", path).Debug()
	req, err := http.NewRequest("GET", path, nil)
	if err != nil {
		return nil, err
	}
	resp, err := d.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode > 399 {
		return nil, errors.New(fmt.Sprintf("error %d in HTTP request: '%s'", resp.StatusCode, resp.Status))
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	log.WithField("Content-Length", resp.ContentLength).Debug()
	return &LayerData{Data: data, Sum: layerSum}, nil
}

func (d *RegistryDownloader) SubmitLayerForCheck(image *Image, layer *LayerData, clairPath string) error {
	path := strings.Join([]string{clairPath, "v1", "layers"}, "/")
	log.WithField("targetURL", path).Debug()
	if layer.Sum == layer.Parent {
		return nil
	}
	postBody := v1.LayerEnvelope{
		Layer: &v1.Layer{
			Name:       layer.Sum,
			ParentName: layer.Parent,
			Path:       strings.Join([]string{image.GetPath(), "blobs", layer.Sum}, "/"),
			Format:     "Docker",
		},
	}
	log.WithField("body", postBody).Debug("Moo")
	jsonBody, err := json.Marshal(postBody)
	if err != nil {
		return err
	}
	log.WithField("layersRequest", postBody).Debug()
	req, err := http.NewRequest("POST", path, bytes.NewBuffer(jsonBody))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := d.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 201 {
		body, _ := ioutil.ReadAll(resp.Body)
		return errors.New(fmt.Sprintf("Clair was unable to process layer %s: '%s'", layer.Sum, body))
	}

	return nil
}

func (d *RegistryDownloader) SubmitImage(image *Image, clairPath string) error {
	sums, err := d.GetLayerSums(image)
	if err != nil {
		return err
	}
	image.Layers = sums
	for _, layer := range sums {
		err := d.SubmitLayerForCheck(image, layer, clairPath)
		if err != nil {
			return err
		}
	}
	return nil
}

func (d *RegistryDownloader) GetLayerVulnerabilities(clairPath string, layer *LayerData) (*v1.LayerEnvelope, error) {
	path := strings.Join([]string{clairPath, "v1", "layers", layer.Sum + "?vulnerabilities"}, "/")
	req, err := http.NewRequest("GET", path, nil)
	if err != nil {
		return nil, err
	}
	log.WithField("URL", path).Debug()

	resp, err := d.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var result v1.LayerEnvelope
	log.Debug(string(respBody))
	err = json.Unmarshal(respBody, &result)
	log.Debug(result.Layer.Features)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (d *RegistryDownloader) GetImageVulnerabilities(clairPath string, image *Image) ([]*v1.Layer, error) {
	var result []*v1.Layer
	for _, layer := range image.Layers {
		env, err := d.GetLayerVulnerabilities(clairPath, layer)
		if err != nil {
			return nil, err
		}
		result = append(result, env.Layer)
	}
	return result, nil
}
