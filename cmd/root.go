package cmd

import (
	"fmt"
	v1 "github.com/coreos/clair/api/v1"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	eclair "gitlab.com/DerDackel/eclair/cmd/internal"
	"os"
)

var rootCmd = &cobra.Command{
	Use:   "eclair",
	Short: "Eclair is a clair scanning CI that does not depend on a docker socket",
	Long: `Eclair tries to solve the problem of interacting with an existing CoreOS Clair 
		   server without having to rely on access to an available docker socket and the privileges
		   associated with it.`,
	Run: func(cmd *cobra.Command, args []string) {

		image, err := eclair.DestructureImage(config.ImagePath)
		if err != nil {
			log.WithError(err).Fatal("Error parsing image name")
		}

		err = downloader.SubmitImage(image, config.ClairUrl)
		if err != nil {
			log.WithError(err).WithFields(log.Fields{
				"Registry": config.ImagePath,
				"Clair":    config.ClairUrl,
			}).Fatal("Unable to scan image")
		}
		var layersWithVulns []*v1.Layer
		for _, layer := range image.Layers {
			result, err := downloader.GetLayerVulnerabilities(config.ClairUrl, layer)
			if err != nil {
				log.WithError(err).Error("Could not get layer vulnerability info")
			}
			layersWithVulns = append(layersWithVulns, result.Layer)
		}
		table := eclair.GenerateVulnerabilityTable(layersWithVulns)
		fmt.Println(table)
	},
}

var config EclairConfig
var downloader eclair.Downloader

type EclairConfig struct {
	ClairUrl  string
	ImagePath string
	AuthToken string
}

func init() {
	rootCmd.PersistentFlags().StringVar(&config.ClairUrl, "clair", "http://localhost:6060", "The URL of the Clair server to use")
	rootCmd.PersistentFlags().StringVar(&config.ImagePath, "image", "", "The full repo and tag of the image you want to scan")
	rootCmd.PersistentFlags().StringVar(&config.AuthToken, "auth", "", "Optional auth token if the registry requires authentication")
	_ = rootCmd.MarkFlagRequired("image")

	downloader = eclair.NewRegistryDownloader(nil)

}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
