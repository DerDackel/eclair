FROM golang:1.12 as builder

ADD . /src
WORKDIR /src

ENV CGO_ENABLED=0
RUN go build -o eclair

FROM scratch

COPY --from=builder /src/eclair /eclair
ENTRYPOINT [ "/eclair" ]