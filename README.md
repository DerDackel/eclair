# Eclair - Clair scanning CLI

__THIS IS NOT YET USABLE!__

This is a POC of a container scanning CLI for Clair that just submits remote layers to a Clair instance.
Inspired by the awesome work of [clair-scanner](https://github.com/arminc/clair-scanner/), this one is designated
for situations where a local docker socket is unavailable, due to security or other reasons. If you have access to a
local or remote Docker socket, please go ahead and use clair-scanner, as it's much more mature.

### State

Total POC, there's no actual CLI yet and tests currently aren't FIRST at all and require an available clair instance as well as a docker registry.
Improving these tests to run as actual unit tests with separate integration tests using testcontainers is high on my
priorities list. Registry authentication isn't yet supported either and I don't know if the clair v1 API even supports
this yet.

### Development

eclair needs the following components to work:

* [clair](https://github.com/coreos/clair)
* [Docker Registry](https://docs.docker.com/registry/)

To provide an environment to work against, there is a `docker-compose.yml` file in the root directory of the repository. To provide an environment you can start it like this:

    docker-compose up

Afterwards clair will need some time to update its vulnerability database. Please wait, until you see a logline with following content:

    {"Event":"update finished","Level":"info","Location":"updater.go:223","Time":"2019-05-28 10:26:42.926436"}

Afterwards you can push an image to the integrated registry. You can use [skopeo](https://github.com/containers/skopeo) to do this in one step:

    skopeo copy docker://ubuntu:bionic-20180426  docker://localhost:5000/ubuntu:latest --dest-tls-verify=false

Now you can try out eclair against this image:

    docker-compose run --rm  eclair --image "registry:5000/ubuntu:latest"